<?php
    session_start();
    require 'database.php';
    $title = $_POST['title'];
    $url = $_POST['url'];

    // Checks if url contains http protocol.
    if (false === strpos($url, '://')) 
    {
        $url = 'http://' . $url;
    }

    $user_id = $_SESSION['user_id'];
    
    $stmt = $mysqli->prepare("insert into posts (title, url, user_id, post_type) values (?, ?, ?, 'link')"); //prepares to enter the post into the database
    if(!$stmt)
    {
    	printf("Query Prep Failed: %s\n", $mysqli->error);
    	exit;
    }
    
    $stmt->bind_param('ssi', $title, $url, $user_id);
    $stmt->execute();
    $stmt->close();
    
    header('Location:index.php');
    exit;
?>


