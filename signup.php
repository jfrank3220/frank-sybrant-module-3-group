<?php
    require 'database.php';
    session_start();

    $user = $_POST['username'];
    $pwd_crypt = crypt($_POST['password']);
    
    $count_stmt = $mysqli->prepare("SELECT COUNT(*) FROM users WHERE username=?");
    if(!$count_stmt)
        {
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
    $count_stmt->bind_param('s', $user);
    $count_stmt->execute();
    $count_stmt->bind_result($cnt);
    $count_stmt->fetch();
    $count_stmt->close();

    if ($cnt == 0) 
    {
        $stmt = $mysqli->prepare("insert into users (username, crypt_pass) values (?, ?)"); //prepares to insert the new user
        if(!$stmt)
        {
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $stmt->bind_param('ss', $user, $pwd_crypt);
        $stmt->execute();

        $stmt->prepare("SELECT users.id FROM users WHERE username=?");
        $stmt->bind_param('s', $user);
        $stmt->execute();
        $stmt->bind_result($user_id);
        $stmt->fetch();

        $_SESSION['user_id'] = $user_id;
        $_SESSION['username']=$user;
        $_SESSION['is_admin']=0;
        
        $stmt->close();

        header('Location:index.php');
        exit();
    } else
    {
        printf("<script type='text/javascript'>
            alert('That username already exists')
            </script>");
        header('Location: login.php');
        exit();
    }

   
?>
