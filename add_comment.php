<?php
	require 'database.php';
	session_start();

	print_r($_POST);

	$post_id = $_POST['post_id'];
	$user_id = $_SESSION['user_id'];
	$text = $_POST['comment_text'];
	
    if(!isset($_SESSION['user_id'])){
        printf("<script type=text/javascript>\n
                alert('You must be logged in to comment')\n
            </script>
            ");
        header('location: login.php');
        exit();
    } else 
    {
    	$stmt = $mysqli->prepare("INSERT INTO comments (post_id, user_id, text) VALUES (?, ?, ?)"); //prepares to enter the comment into the comments database
    	$stmt->bind_param('sss', $post_id, $user_id, $text);
    	$stmt->execute();
    	$stmt->close();
    	header('location: comments.php?post_id='.$post_id);
    	exit();
    }	
?>