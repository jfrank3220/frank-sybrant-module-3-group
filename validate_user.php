<?php
require 'database.php';

session_start();
 
// Use a prepared statement
$stmt = $mysqli->prepare("SELECT COUNT(*), id, crypt_pass, is_admin FROM users WHERE username=?");
 
// Bind the parameter
$stmt->bind_param('s', $user);
$user = $_POST['username'];
$stmt->execute();
 
// Bind the results
$stmt->bind_result($cnt, $user_id, $pwd_hash, $is_admin);
$stmt->fetch();
 
$pwd_guess = $_POST['password'];
// Compare the submitted password to the actual password hash
if( $cnt == 1 && crypt($pwd_guess, $pwd_hash)==$pwd_hash){
	// Login succeeded!
	$_SESSION['user_id'] = $user_id;
	$_SESSION['username'] = $user;
	$_SESSION['is_admin'] = $is_admin;
	// Redirect to your target page
	header('location: index.php');
	exit();
}else{
	// Login failed; redirect back to the login screen
	header('location: login.php');
	exit();
}
?>