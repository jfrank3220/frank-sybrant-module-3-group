<!DOCTYPE html>
<html>
<head>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
	<title>Majical News Site</title>
	<meta charset="utf-8">
	<link href="style.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>

<body>
	<div class="container">
	<div class="header">
		<?php include 'user_header.php'; ?>
	</div>
	<?
		require 'database.php'; //connects to mysql

		$stmt = $mysqli->prepare("SELECT posts.id, title, url, users.username, posts.date_created FROM posts JOIN users ON (posts.user_id=users.id) ORDER BY posts.date_created DESC"); //sets up the query to select the posts to show
	    if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
	    }

		$stmt->execute();
		$stmt->bind_result($post_id, $title, $url, $author, $date); 
		echo "<div class='post_container'>\n";
		while ($stmt->fetch()) 
		{ 
			printf("<div class='post'> 
					<a href='%s'>%s</a><br>
					<small>
						Created by %s at %s
					</small><br>
					<div class='comment_form'>
					<form action='comments.php' method='GET'>
						<input type='hidden' name='post_id' value='%s'>
						<input type='submit' value='comments'>
					</form>
					</div>
				", $url, $title, $author, $date, $post_id);//shows the post and the comments button

			// Only allow users who are logged in can delete or edit posts
			if (isset($_SESSION['username'])) 
			{
				// Only allow users to delete and edit their own posts
				if ($author == $username || $is_admin==1) 
				{
					printf("<div class='delete_form'>
					        <form action='delete_post.php' method='POST'>\n
						<input type='hidden' name='post_id' value='%s'>\n
						<input type='submit' value='delete'>
						</form></div>					
						", $post_id);
					printf("<div class='edit_form'>
					        <form action='edit_post.php' method='POST'>
						<input type='hidden' name='post_id' value='%s'>\n
						<input type='submit' value='edit'>
						</form></div>					
						", $post_id);
				} //adds the edit and delete button if the user is the owner
			}
			echo "\n</div>";
		}
		echo "\n</div>";
		$stmt->close();
	?>
	</div>
</body>