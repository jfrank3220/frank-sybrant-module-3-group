<!DOCTYPE html>
<html>
<head>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
	<title>Majical News Site</title>
	<meta charset="utf-8">
	<link href="style.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class='container'>
	<div class="header">
		<?php include 'user_header.php'; ?>
	</div>
	<?
		require 'database.php';//connects to mysql

		$post_id = $_GET['post_id'];

		$stmt = $mysqli->prepare("SELECT posts.user_id, posts.post_type, posts.title, posts.url, posts.text FROM posts WHERE posts.id=?");//prepares query to get the post content
		if(!$stmt)
		{
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		$stmt->bind_param('s', $post_id);
		$stmt->execute();
		$stmt->bind_result($author_id, $post_type, $title, $url, $content);
		$stmt->fetch();
		$stmt->close();

		if ($post_type=='text') //displays a text post
		{
			echo "<div id='text_post_comments'><h3>".$title."</h3><p>".$content."</p></div>";
		}
		if ($post_type=='link') { //displays a link post
			printf("<h3>%s</h3><a href='%s'>%s</a>", $title, $url, $url);
		}
		printf("
			<div class='comment'>
			<form action='add_comment.php' method='POST' id='add_comment'>
				Add comment<br>
				<textarea name='comment_text' rows='8' cols='100' form='add_comment'></textarea><br>
				<input type='hidden' value='%s' name='post_id'>
				<input type='submit' value='Submit' name='submit'>
			</form>
			</div>
			", $post_id);

		$comment_stmt = $mysqli->prepare("SELECT comments.id, comments.text, users.username, comments.date_created from comments join users on (comments.user_id=users.id) where comments.post_id=?"); //prepares query to get the comments
		if(!$comment_stmt)
		{
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		$comment_stmt->bind_param('s', $post_id);
		$comment_stmt->execute();
		$comment_stmt->bind_result($comment_id, $comment_text, $comment_author, $date);

		while ($comment_stmt->fetch()) //gets the comment associated with the post
		{
			echo "<div class='post_comment'>\n";
				echo "<p>\n";

				echo $comment_text."<br>";
				echo "<small>Created by ".$comment_author." at ".$date."</small><br>";

				// Only allow users who are logged in can delete or edit comments
				if (isset($_SESSION['username'])) 
				{
					// Only allow users to delete and edit their own comments
					if ($comment_author == $username || $is_admin) 
					{
						printf("<div class='delete_form'>
							<form action='delete_comment.php' method='POST'>\n
							<input type='hidden' name='comment_id' value='%s'>\n
							<input type='submit' value='delete'>\n
							</form></div>\n					
							", $comment_id);
						printf("<div class='edit_form'>
							<form action='edit_comment.php' method='POST'>
							<input type='hidden' name='comment_id' value='%s'>\n
							<input type='submit' value='edit'>\n
							</form></div>\n					
							", $comment_id);
					}
				}

				echo "</p>\n";
			echo "</div>\n";
		}
		echo "\n"
	?>

	</div>
</body>
