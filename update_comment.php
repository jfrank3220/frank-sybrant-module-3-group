<?php
	require 'database.php';
	session_start();

	$comment_id = $_POST['comment_id'];
	$text = $_POST['comment_text'];

	$stmt = $mysqli->prepare("UPDATE comments SET comments.text=? WHERE comments.id=?");
	$stmt->bind_param('ss', $text, $comment_id);
	$stmt->execute();
	$stmt->close();

	header('location: index.php');
	exit();
?>