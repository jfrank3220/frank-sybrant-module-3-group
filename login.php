<!DOCTYPE html>
<html>
<head>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <meta charset="UTF-8">
    <title>Majical News Site</title>
    <link href="style.css" rel="stylesheet" type="text/css">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div class='container'>
    <div class="header">
        <?php include 'user_header.php'; ?>
    </div>
    <?php
        if (isset($_SESSION['user_id'])) 
        {
            header("location: index.php");
            exit();
        }
    ?>
    
    <div id="sign_in"> 
    <form action="validate_user.php" method="POST">
        <p>Sign in to your account with your username and password.</p>
        Username: <input type="text" name="username" class="usernameinput"/><br>
        Password: <input type="password" name="password" class="passwordinput" /><br>
        <input type="submit" value="Log In" name="login" />
    </form>
    </div>
    
    <div id="sign_up">
    <form action="signup.php" method="POST">
        <p>Don't have an account? Create a username and password.</p>
        Username: <input type="text" name="username" class="usernameinput"/><br>
        Password: <input type="password" name="password" class="passwordinput"/><br>
        <input type="submit" value="Sign Up" name="signup" />
    </form>
    </div>    
    
    </div>
</body>
</html>