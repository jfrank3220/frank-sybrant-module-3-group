<?php
    require 'database.php';
    
    $comment_id = $_POST['comment_id'];

    $stmt = $mysqli->prepare("delete from comments where comments.id=?"); //deletes the comment we are currently on
    if(!$stmt)
    {
	   printf("Query Prep Failed: %s\n", $mysqli->error);
	   exit;
    }
    
    $stmt->bind_param('s', $comment_id);
    $stmt->execute();
    $stmt->close();
    
    header('Location:index.php');
    exit;

?>