<?php
    require 'database.php';
    
    $post_id = $_POST['post_id'];

    $stmt = $mysqli->prepare("delete from posts where posts.id=?"); //deletes the post we are currently on
    if(!$stmt)
    {
	   printf("Query Prep Failed: %s\n", $mysqli->error);
	   exit;
    }
    
    $stmt->bind_param('s', $post_id);
    $stmt->execute();
    $stmt->close();
    
    header('Location:index.php');
    exit;
?>