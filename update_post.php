<?php
	require 'database.php';
	session_start();

	$post_id = $_POST['post_id'];
	$title = $_POST['title'];
	$url = $_POST['url'];
	$content = $_POST['post_content'];

	$stmt = $mysqli->prepare("UPDATE posts SET posts.title=?, posts.url=?, posts.text=? WHERE posts.id=?");
	$stmt->bind_param('ssss', $title, $url, $content, $post_id);
	$stmt->execute();
	$stmt->close();

	header('location: index.php');
	exit();
?>