<!DOCTYPE html>
<html>
<head>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <meta charset="UTF-8">
    <title>Majical News Site</title>
    <link href="style.css" rel="stylesheet" type="text/css">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div class='container'>
    <div class="header">
        <?php include 'user_header.php'; ?>
    </div>
    <?
        if(!isset($_SESSION['user_id'])){
            printf("
                <script type=text/javascript>\n
                    alert('You must be logged in to submit')\n
                </script>
                ");
            header("location: login.php");
            exit();
        }
        $user_id = $_SESSION['user_id']; //checks to see if the user is logged in and then lets them create a text or link post
        printf("
            <div id='create_text'>
            <h2>Create a text post</h2>
            <div class='create_post_form'>
            <form action='add_text_post.php' method='POST' id='text_post' class='text_post'>
                Title<br><input type='text' name='title' class='post_title'/><br>
                Content<br>
                <textarea rows='8' cols='30' form='text_post' name='post_content' id='create_a_text_post'></textarea><br>
                <input type='submit' value='Post' name='post' /><br>
            </form>
            </div>
            </div>
    
            <div id='create_link'>
            <h2>Create a link post</h2>
            <div class='create_post_form'>
            <form action='add_link_post.php' method='POST'>
                Title<br><input type='text' name='title' class='post_title'/><br>
                Url<br><input type='text' name='url' class='post_url'/><br>
                <input type='submit' value='Post' name='post' /><br>
            </form>
            </div>
            </div>
        ");
    ?>
    </div>
</body>
</html>