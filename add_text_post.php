<?php
    session_start();
    require 'database.php';
    $title = $_POST['title'];
    $content = $_POST['post_content'];
    $user_id = $_SESSION['user_id'];
    
    $result = mysqli_query($mysqli, "SHOW TABLE STATUS LIKE 'posts'");
    $row = mysqli_fetch_array($result);
    $nextId = $row['Auto_increment'];
    
    $post_url='http://ec2-54-227-138-228.compute-1.amazonaws.com/~jordan/frank-sybrant-module-3-group/comments.php?post_id='.$nextId;

    $stmt = $mysqli->prepare("insert into posts (title, text, url, user_id, post_type) values (?, ?, ?, ?, 'text')"); //prepares to insert the text post into the database
    if(!$stmt){
	   printf("Query Prep Failed: %s\n", $mysqli->error);
	   exit();
    }
    
    $stmt->bind_param('sssi', $title, $content, $post_url, $user_id);
    $stmt->execute();
    $stmt->close();
    
    header('Location:index.php');
    exit;
?>