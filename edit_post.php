<!DOCTYPE html>
<html>
<head>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
	<title>Majical News Site</title>
	<link href="style.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class='container'>
	<div class='header'>
		<?php include 'user_header.php'; ?>
	</div>

	<?
		require 'database.php';

		$post_id = $_POST['post_id'];
		$stmt = $mysqli->prepare("SELECT posts.user_id, posts.post_type, posts.title, posts.url, posts.text FROM posts WHERE posts.id=?");
		$stmt->bind_param('s', $post_id);
		$stmt->execute();
		$stmt->bind_result($author_id, $post_type, $title, $url, $content);
		$stmt->fetch();

		if (!isset($user_id) || ($user_id != $author_id && $is_admin!=1)) {
			header('location: login.php');
			exit();
		}

		if ($post_type=="link") 
		{
			printf("
				<form action='update_post.php' method='POST'>
                	Title:<br><input type='text' name='title' value='%s'/><br>
                	Url:<br><input type='text' name='url' value='%s'/><br>
                	<input type='hidden' value='%s' name='post_id'><br>
                	<input type='hidden' value='%s' name='post_content'>
                	<input type='submit' value='Save' name='save' /><br>
            	</form>", $title, $url, $post_id, $content); 

		} elseif ($post_type=="text") 
		{
			printf("
			       <div class='comment'>
				<form action='update_post.php' method='POST' id='edit_post'>
					Title<br><input type='text' name='title' value='%s'/><br>
	                Content<br>
	                <textarea rows='10' cols='200' form='edit_post' name='post_content'>%s</textarea><br>
	                <input type='hidden' value='%s' name='post_id'>
	                <input type='hidden' name='url' value='%s'/><br>
	                <input type='submit' value='Post' name='post' /><br>
	            </form></div>", $title, $content, $post_id, $url);
		}
	?>
	</div>
</body>
</html>
