<!DOCTYPE html>
	<html>
<head>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
	<title>Majical News Site</title>
	<link href="style.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class='container'>
	<div class='header'>
		<?php include 'user_header.php'; ?>
	</div>
	<?
		require 'database.php';

		$comment_id = $_POST['comment_id'];
		$stmt = $mysqli->prepare("SELECT comments.user_id, comments.text FROM comments WHERE comments.id=?"); //gets the comment
		if(!$stmt)
		{
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		$stmt->bind_param('s', $comment_id);
		$stmt->execute();
		$stmt->bind_result($author_id, $text);
		$stmt->fetch();
		$stmt->close();

		if (!isset($user_id) || ($user_id != $author_id && $is_admin!=1)) 
		{
			header('location: login.php');
			exit();
		}
		//prints the comment content
		printf("<div class='comment'>
		        <form action='update_comment.php' method='POST' id='edit_comment'>
			<textarea name='comment_text' rows='8' cols='30' form='edit_comment'>%s</textarea>
			<input type='hidden' value='%s' name='comment_id'><br>
			<input type='submit' value='Save'>
			</form>
			</div>
			", $text, $comment_id);
	?>
	</div>
</body>
</html>
